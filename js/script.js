function second_video($status) {
	
    //'use strict';
    if(!$status) $status = 'none';
    
    var bodyEl = document.body,
		videoWrap = document.querySelector('.video-wrap');

    if($status == 'video'){
    var videoEl = videoWrap.querySelector('video'),
		closeCtrl = document.querySelector('.action--close');    
    } else {
        var closeCtrl = document.querySelector('.action--close');  
    }
    
	function init() {
		initEvents();
	}

	function initEvents() {
		if($status != 'none'){ closeCtrl.addEventListener('click', hide); }
        if($status == 'video'){
		videoEl.addEventListener('canplaythrough', allowPlay);
		videoEl.addEventListener('ended', hide);}
	}

	function allowPlay() {
		classie.add(bodyEl, 'video-loaded');
	}

	function hide() {
		classie.remove(videoWrap, 'video-wrap--show');
		classie.add(videoWrap, 'video-wrap--hide');
		if($status == 'video'){videoEl.pause();}
	}
    
    function play() {
        hide();
		if($status == 'video'){videoEl.currentTime = 0;}
		classie.remove(videoWrap, 'video-wrap--hide');
		classie.add(videoWrap, 'video-wrap--show');
		if($status == 'video'){setTimeout(function() {videoEl.play();}, 600);}
	}

	init();

   if($status != 'none'){ play(); }
};


$(function() {
    
    function play($id){        
    var $content = '';
    if( $id==1) $content = '<img src="images/first_bg.jpg" alt="" />';
    if( $id==2) $content = '<img src="images/second_bg.jpg" alt="" />';
    if( $id==3) $content = '<img src="images/third_bg.jpg" alt="" />';
    if( $id==4) $content = '<video class="video-player" id="second_video" src="video/woods.mp4" poster="images/woods.jpg" preload="auto"><source src="video/woods.webm" type=\'video/webm; codecs="vp8.0, vorbis"\'><source src="video/woods.ogg" type=\'video/ogg; codecs="theora, vorbis"\'><source src="video/woods.mp4" type=\'video/mp4; codecs="avc1.4D401E, mp4a.40.2"\'><p>Sorry, but your browser does not support this video format.</p></video>';
    if( $id==5) $content = '<video class="video-player" id="second_video" src="video/woods.mp4" poster="images/fourth_bg.jpg" preload="auto"><source src="video/woods.webm" type=\'video/webm; codecs="vp8.0, vorbis"\'><source src="video/woods.ogg" type=\'video/ogg; codecs="theora, vorbis"\'><source src="video/woods.mp4" type=\'video/mp4; codecs="avc1.4D401E, mp4a.40.2"\'><p>Sorry, but your browser does not support this video format.</p></video>';
    if( $id==6) $content = '<video class="video-player" id="second_video" src="video/woods.mp4" poster="images/second_bg.jpg" preload="auto"><source src="video/woods.webm" type=\'video/webm; codecs="vp8.0, vorbis"\'><source src="video/woods.ogg" type=\'video/ogg; codecs="theora, vorbis"\'><source src="video/woods.mp4" type=\'video/mp4; codecs="avc1.4D401E, mp4a.40.2"\'><p>Sorry, but your browser does not support this video format.</p></video>';
    
    $content +='<button class="action action--close"><i class="fa fa-close"></i></button>';
      
    $('#second_modal_source').html($content);
        
    if($id > 3){
    second_video('video');   
    } else {
    second_video('photo');
    }
      
    var $prev, $next;
        
    if($id == 1){ $prev = 6;}else{$prev = $id - 1;}
    if($id == 6){ $next = 1;}else{$next = $id + 1;}
        
    $('#second_video_prev').click(function(){play($prev)});
    $('#second_video_next').click(function(){play($next)})    
    
    }
    
    $('#second_modal_1').click(function(){play(1);});
    $('#second_modal_2').click(function(){play(2);});
    $('#second_modal_3').click(function(){play(3);});
    $('#second_modal_4').click(function(){play(4);});
    $('#second_modal_5').click(function(){play(5);});
    $('#second_modal_6').click(function(){play(6);});

});


function modalopen(_content){
    $('#modal_source').html(_content); 
    $('.videoclose').click(function(){ modalclose()});
    $('#modal_source').css('display', 'block');
    $('#modal').css('display', 'block');
    $('#modal_fon').css('display', 'block');
}

function modalclose(){
    $('#modal_source').html(''); 
    $('#modal_source').css('display', 'none');
     $('#modal').css('display', 'none');
    $('#modal_fon').css('display', 'none');
}

$('#modal_fon').click(function(){ modalclose()});

$('#first_video').click(function(){     
    modalopen('<div id="mainVideo_cont"><div id="mainVideo_border"><div class="first_video_left videoclose"></div><div class="first_video" id="mainVideo"></div><div class="first_video_right videoclose"></div></div></div>'); 
    jwplayerInContent = jwplayer("mainVideo")
        .setup({
            modes: [{ type: 'flash', src: 'jwplayer/jwplayer.flash.swf' }],
            image: "images/poster.jpg",
            file: "videos/bunchems.mp4",
            skin:"stormtrooper",
            width: "50%",
            aspectratio: "5:2.81",
            autostart: true
        });
});

$(".form_input").click(function() {
    if($(".name_input").val()=="" || $(".phone_input").val()=="" || $(".phone_input").val()=="+7(___)___-__-__") {
        alert('Заполните все поля!');
    }
});

$(".form_input").mouseover(function() {
    _mask=$(".phone_input").val();
    _mask=_mask.substr(-1);
    if($(".name_input").val()!="" && $(".phone_input").val()!="" && $(".phone_input").val()!="+7(___)___-__-__" && _mask!='_'){
	   $('.form_input').removeAttr('type');
	   $('.form_input').attr('type','submit');
    }
});