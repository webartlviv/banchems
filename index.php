<?php include('head.php'); ?>

<div class="modal_fon" id="modal_fon"></div>
<div style="display:none;" id="modal_fon_num"></div>
<span style="display:none;" id="modal_span_prev"></span>
<span style="display:none;" id="modal_span_next"></span>	
<div class="modal" id="modal">
    <div class="modal_prev" id="modal_prev"></div>
    <div class="modal" id="modal_source"></div>
    <div class="modal_next" id="modal_next"></div>
</div>

<div class="video-wrap">
    <div class="video-inner"><div id="second_modal_source"></div><div class="prev pointer" id="second_video_prev"><img src="images/arrow_prev.png" alt="" /></div><div class="next pointer" id="second_video_next"><img src="images/arrow_next.png" alt="" /></div></div> <!-- /video-inner -->
</div><!-- /video-wrap-->

<div class="down_menu Rumpelwhite" id="fixed_menu">
   <div class="content">
        <img class="logo inline logo_menu" id="logo_menu" src="images/logo.png" alt="Bunchems!" />
        <ul class="menu">
            <li><a href="#gallery">для чего банчемсы?</a></li>
            <li><a href="#set">что в наборе?</a></li>
            <li><a href="#why_we">почему мы?</a></li>
            <li><a href="#buy_bunchems">купить банчемсы</a></li>
            <li>внимание!</li>
            <li class="phone">+7 (000) 000-00-00</li>
        </ul>
    </div>
</div>

<!-- SVG Sprite -->
	<div style="height: 0; width: 0; position: absolute; visibility: hidden;" aria-hidden="true">
		<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" focusable="false">
			<symbol id="ripply-scott" viewBox="0 0 100 100">
				<circle id="ripple-shape" cx="1" cy="1" r="1" />
			</symbol>
		</svg>
	</div>
	<!-- /end sprite -->

<div class="gen_wrapper">
<div class="pages" id="fullpage">
<section class="first Rumpelwhite section ">
   <img class="bg" src="images/first_bg.jpg" alt="" />
    <div class="head">
        <div class="medals float_left">
            <img src="images/besttoys2015.png" alt="BestToys 2015" />
            <img src="images/toty2016.png" alt="TOTY 2016" />
        </div>
        <div class="logo float_left">
            <span class="logotext block">конструктор-липучка</span>
            <img class="block" src="images/logo.png" alt="Bunchems!" />
        </div>
        <div class="contact float_right">
            <span class="phone block">+7 (000) 000-00-00</span>
            <button class="buynow block yelowbutton Rumpelwhite button styl-material" id="js-ripple-btn">заказать набор<br>bunchems
                <svg class="ripple-obj" id="js-ripple">
					<use height="100" width="100" xlink:href="#ripply-scott" class="js-ripple"></use>
				</svg>
            </button>
        </div>
    </div>
    <main class="first hidden">
        <div class="social">
            <span class="title">липучки в<br>социальных сетях</span>
            <img class="pointer social_bunch" src="images/sok.png" alt="" />
            <img class="pointer social_bunch" src="images/svk.png" alt="" />
            <img class="pointer social_bunch" src="images/sut.png" alt="" />
        </div>
        <div class="center">
            <span class="title">Что такое<br>bunchems?</span>
            <div class="play"><img class="pointer" id="first_video" src="images/play.png" alt="" onclick="first_video()" /></div>
            <div class="down">
                <span class="title">Лучшая игрушка года - конструктор 
Bunchems уже в России!</span>
            </div>
        </div>
        <div class="rights">
            <span>2016. Все права защищены!</span>
        </div>
    </main>
</section>

<section class="second Rumpelwhite section ">
   <img class="bg" src="images/second_bg.jpg" alt="" />
    <main>
       <div class="photostik photo1 pointer"><img src="images/second_photostick_photo1.png" alt="" id='second_modal_1' class="photo"></div>
       <div class="photostik photo2 pointer"><img src="images/second_photostick_photo2.png" alt="" id='second_modal_2' class="photo"></div>
       <div class="photostik photo3 pointer"><img src="images/second_photostick_photo3.png" alt="" id='second_modal_3' class="photo"></div>
       <div class="photostik video1 pointer"><img src="images/second_photostick_video1.png" alt="" id='second_modal_4' class="video"></div>
       <div class="photostik video2 pointer"><img src="images/second_photostick_video2.png" alt="" id='second_modal_5' class="video"></div>
       <div class="photostik video3 pointer"><img src="images/second_photostick_video3.png" alt="" id='second_modal_6' class="video"></div>
        <div class="hash_bunchems">
            <img class="hashlogo" src="images/hashlogo.png" alt="">
        </div>
        <div class="social">
            <span>веселые и забавные примеры в соц. сетях</span>
        </div>
        <div class="title">
            <span>разыскивает<br>воображение</span>
        </div>
        <div class="ul">
            <ul>
                <li><img src="images/second_ul_li.png" alt="" />Развивает фантазию</li>
                <li><img src="images/second_ul_li.png" alt="" />Развивает образное мышление</li>
                <li><img src="images/second_ul_li.png" alt="" />Развивает мелкую моторику рук</li>
            </ul>
        </div>
        <div class="button">
            <button class="buynow yelowbutton Rumpelwhite button styl-material" id="js-ripple-btn2">заказать<br>Bumchems
                <svg class="ripple-obj" id="js-ripple2">
					<use height="100" width="100" xlink:href="#ripply-scott" class="js-ripple"></use>
				</svg>
            </button>
        </div>
    </main>
    <div class="rights">
        <span>2016. Все права защищены!</span>
    </div>
</section>

<section class="third Rumpelwhite section " id="buyform">
   <img class="bg" src="images/third_bg.jpg" alt="" />
    <main class="third">
        <div class="center">
           <img class="form_bg" src="images/third_form_bg.png" alt="" />
           <div class="title">для покупки банчемсов<br>за</div>
            <div class="price">2990<span class="rub">р</span></div>
                <form action="thanks.php" method="post" class="buy">
                    <div class="title">отправьте нам<br>заполненную форму</div>
                    <div class="form">
                        <input type="text" name="name" class="name_input" placeholder="Ваше имя">
                        <input type="phone" name="phone" class="phone_input" placeholder="Ваш телефон">
                        <input type="text" name="promo" class="none" placeholder="промо-код">
                        <input type="button" name="enter_buy" class="Rumpelwhite pointer form_input" value="сделать заказ">
                    </div>
                </form>
        <div class="winner">
            <div class="title">игрушка победитель<br>всего что можно</div>
            <div class="medals">
                <img src="images/besttoys2015.png" alt="BestToys 2015" />
                <img src="images/toty2016.png" alt="TOTY 2016" />
            </div>            
        </div>
        </div>
        <div class="promo">
            <div class="title">получить промо-код для покупки банчемс</div>
               <img class="promo_bg" src="images/third_promo_bg.png" alt="" />
                <div class="form">
                    <div class="text">Обязательно оставьте комментарий  в соц сетях "Я решила купить эту прикольную игрушку своему ребенку и настроение явно поднялось.</div>
                    <div class="input"><input type="text" placeholder="Ваш E-mail"></div>
                        <div class="socialtext">Обязательно оставьте комментарий  в соц сетях "Я решила купить эту прикольную игрушку своему ребенку и настроение явно поднялось. И ссылка на сайт." (или что-то в этом плане). </div>
                        <div class="social">
                            <img class="pointer social_bunch" src="images/sok.png" alt="" />
                            <img class="pointer social_bunch" src="images/svk.png" alt="" />
                            <img class="pointer social_bunch" src="images/sut.png" alt="" />
                        </div>                    
                </div>            
        </div>
    </main>
    <div class="rights">
        <span>2016. Все права защищены!</span>
    </div>
</section>

<section class="fourth Rumpelwhite section ">
    <img class="bg" src="images/fourth_bg.jpg" alt="" />
    <main>
        <div class="title">Что нужно, чтобы <br>купить Bunchems?</div>
        <div class="content">
            <div class="line">
                <div class="item"><img class="liimg" src="images/fourth_li_left.png" alt="" /><span>Вы отправляете заявку <br>на нашем сайте <br>заполнив форму</span></div>
                <div class="item"><img class="liimg" src="images/fourth_li_left.png" alt="" /><span>Наш менеджер <br>перезвонит <br>вам и ответит <br>на ваши вопросы</span></div>
                <div class="item"><img class="liimg" src="images/fourth_li_left.png" alt="" /><span>При необходимости <br>уточним <br>детали или <br>отправим e-mail</span></div>
            </div>
            <div class="text first"><img class="liimg" src="images/fourth_li_left_down.png" alt="" /><span>Если вас все устраивает отправляем <br>товар курьером или почтой</span></div>
            <div class="text second"><img class="liimg" src="images/fourth_li_left_down.png" alt="" /><span>Вы получаете товар и <br>оплачиваете его при получении</span></div>
        </div>
        <div class="info">курьерская доставка по Москве от 300 рублей <br>отправка почтой России от 400 рублей</div>  
    </main>
    <div class="rights">
        2016. Все права защищены!
    </div>
</section>

<section class="fifth Rumpelwhite section">
   <img class="bg" src="images/fifth_bg.jpg" alt="" />
    <main>
        <div class="title"><span>Что входит в набор<br>Bunchems?</span></div>
        <div class="content"><div class="left">
            <div class="item">
               <img class="cloud" src="images/fifth_cloud.png" alt="" />
                <div class="title">370</div>
                <div class="descr">шариков-липучек</div>
                <div class="text">красные синие<br>фиолетовые зеленые<br>оранжевые желтые<br>черные</div>
            </div>
             <div class="item">
               <img class="cloud" src="images/fifth_cloud.png" alt="" />
                <div class="title">36</div>
                <div class="descr">Аксессуаров:</div>
                <div class="text">ножки, ручки, усы<br>котелки, глазки, крылышки</div>
            </div>
        </div>
        <div class="right">
            <div class="item">
               <img class="cloud" src="images/fifth_cloud.png" alt="" />
                <div class="title">15</div>
                <div class="descr">Инструкций<br>по сборке фигурок</div>
            </div>
            <div class="item">
               <img class="cloud" src="images/fifth_cloud.png" alt="" />
                <div class="descr">тысячи созданных<br>персонажей и вещей</div>
            </div>
        </div>
        </div>
        <div class="center">
            <div class="button">
            <button class="buynow yelowbutton Rumpelwhite button styl-material" id="js-ripple-btn3">заказать<br>Bumchems
                <svg class="ripple-obj" id="js-ripple3">
					<use height="100" width="100" xlink:href="#ripply-scott" class="js-ripple"></use>
				</svg>
            </button>
        </div>
        </div>
   </main>
    <div class="rights">
        <span>2016. Все права защищены!</span>
    </div>
</section>

<section class="sixth Rumpelwhite section">
    <img class="bg" src="images/sixth_bg.jpg" alt="" />
    <main style="height:100%">
        <div class="title">
            <span>отзывы<br>очень довольных родителей</span>
        </div>
        <div class="content">
            <div class="left"><img class="arrovs left owl-carousel-next" src="images/sixth_arrpw_prev.png" alt="" /></div>
            <div class="center">
            <img class="book" src="images/sixth_book.png" alt="" />
            <div id="owl-example" class="owl-carousel">
            <div class="item">
                <div class="left">
                    <div class="title">Родители Вани</div>
                    <div class="content">Идейные соображения высшего порядка, а также дальнейшее развитие различных форм деятельности способствует подготовки и реализации дальнейших направлений развития. С другой стороны начало повседневной работы по формированию позиции позволяет выполнять важные задания по разработке системы обучения кадров, соответствует насущным потребностям.
                    <br><br>СПАСИБО :) :) :) :)</div>
                </div>
                <div class="right">
                    <div class="title">Родители Вани</div>
                    <div class="content">Идейные соображения высшего порядка, а также дальнейшее развитие различных форм деятельности способствует подготовки и реализации дальнейших направлений развития. С другой стороны начало повседневной работы по формированию позиции позволяет выполнять важные задания по разработке системы обучения кадров, соответствует насущным потребностям.</div>
                </div>
            </div>
            <div class="item">
                <div class="left">
                    <div class="title">Родители Вани</div>
                    <div class="content">Идейные соображения высшего порядка, а также дальнейшее развитие различных форм деятельности способствует подготовки и реализации дальнейших направлений развития. С другой стороны начало повседневной работы по формированию позиции позволяет выполнять важные задания по разработке системы обучения кадров, соответствует насущным потребностям.
                    <br><br>СПАСИБО :) :) :) :)</div>
                </div>
                <div class="right">
                    <div class="title">Родители Вани</div>
                    <div class="content">Идейные соображения высшего порядка, а также дальнейшее развитие различных форм деятельности способствует подготовки и реализации дальнейших направлений развития. С другой стороны начало повседневной работы по формированию позиции позволяет выполнять важные задания по разработке системы обучения кадров, соответствует насущным потребностям.</div>
                </div>
            </div>
            </div>
            </div>
            <div class="right"><img class="arrovs right owl-carousel-prev" src="images/sixth_arrpw_next.png" alt="" /></div>
        </div>
     </main>
    <div class="rights">
        <span>2016. Все права защищены!</span>
    </div>
</section>

<section class="seventh Rumpelwhite section">  
    <img class="bg" src="images/seventh_bg.jpg" alt="" />
    <main class="center" style="height:100%">  
        <div class="title">
            <span>У Вас остались вопросы?<br>перезвонить Вам?</span>
            <div class="button"><input type="button" name="enter_buy" class="Rumpelwhite pointer form_input green_button" value="перезвонить мне"></div>
        </div>
        <div class="content">
            <div class="left">
                <div class="title">Для оптовых<br>покупателей</div>
                <div class="button"><input type="button" name="enter_buy" class="Rumpelwhite pointer form_input green_button" value="форма заказа"></div>
            </div>
            <div class="right">
                <div class="title">обратная<br>связь</div>
                <div class="button"><input type="button" name="enter_buy" class="Rumpelwhite pointer form_input green_button" value="написать нам"></div>
            </div>
        </div>
        <div class="bottom">
            <div class="social">
                <div class="title">липучки в социальных сетях:</div>
                <div class="social_buttons">
                        <img class="pointer social_bunch" src="images/sok.png" alt="" />
                        <img class="pointer social_bunch" src="images/svk.png" alt="" />
                        <img class="pointer social_bunch" src="images/sut.png" alt="" />
                </div>
            </div>
            <div class="info">
                <div class="title">Конструкторы Банчемс (Bunchems) в России.</div>
                <div class="text">Отправка Бунчемс по всей России (Москва, Санкт-Петербург, Екатеринбург, Подольск, Нижний Новгород, Волгоград). Курьерская доставка конструктор-липучка Bunchems (Банчемс) по Москве. Доставка Bunchems по Москве.<br>У нас можно купить детский набор для творчества, а также купить Банчемс (Bunchems) в России. Продажа, цена на конструктор Банчемс (Bunchems) в России оптом. Купить Банчемс в Москве. Банчемс купить оптом.</div>
            </div>
            <div class="rights">
                <ul>
                    <li>ИП Бред Питт.</li>
                    <li>2016. Все права защищены!</li>
                    <li><a href="#">Политика конфиденциальности</a></li>
                </ul>
            </div>
        </div>
    </main>
</section>

</div>
</div>
<?php include('counters.php'); ?>