<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Bunchems</title>
	<meta name="description" content="Bunchems">
	<meta name="keywords" content="Bunchems">
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>
		
	<!--meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="viewport" content="width=device-width, user-scalable=no"-->

	<link rel="stylesheet" href="css/style.css">	
	<link rel="stylesheet" href="css/responsive.css">	
		
	<link rel="stylesheet" type="text/css" href="css/normalize.css" />
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.3.0/css/font-awesome.min.css" />
	
	<script src="js/jquery-1.11.1.min.js"></script>
    <script src="js/jquery.maskedinput.min.js"></script>
    <script>jQuery(function($){
	 $("input[name='phone']").mask("+7(999)999-99-99");
	});
    </script>
    <link rel="stylesheet" type="text/css" href="css/jquery.fullpage.css" />

    <script type="text/javascript" src="js/jquery.fullpage.js"></script>

    <!-- Important Owl stylesheet -->
  <link rel="stylesheet" href="owl-carousel/owl.carousel.css">
   
  <!-- Default Theme -->
  <link rel="stylesheet" href="owl-carousel/owl.theme.css">
      
  <!-- Include js plugin -->
  <script src="owl-carousel/owl.carousel.js"></script>

    <script>$(document).ready(function() {
	   $('#fullpage').fullpage({
           anchors: ['gen', 'gallery', 'buy_bunchems', 'why_we', 'set', 'reviews', 'questions'],
           onLeave: function(index, nextIndex, direction){
               if(index == 1 && direction =='down'){
                   $('#fixed_menu').slideDown(1000);
               }
               else if(index != 1 && nextIndex == 1 && direction =='up'){$('#fixed_menu').slideUp(700);
               }
               else{
                   $('#fixed_menu').slideDown(1000); 
               }
           },
           scrollingSpeed: 1000
	   });
    });
    </script>
    <script>
        $(document).on('click', '#js-ripple-btn', function(){ 
        setTimeout(function(){ $.fn.fullpage.moveTo('buy_bunchems', 1); }, 500);    
    });
        $(document).on('click', '#js-ripple-btn2', function(){
        setTimeout(function(){ $.fn.fullpage.moveTo('buy_bunchems', 1); }, 500);
    });
        $(document).on('click', '#js-ripple-btn3', function(){
        setTimeout(function(){ $.fn.fullpage.moveTo('buy_bunchems', 1); }, 500);
    });
        $(document).on('click', '#logo_menu', function(){
        setTimeout(function(){ $.fn.fullpage.moveTo('gen', 1); }, 500);
    });
    </script>
    <script>$(document).ready(function() {
 
    var owl = $("#owl-example");
    
    owl.owlCarousel({
      items: 1,
      autoPlay : 5000,
      
    });

    $(".owl-carousel-next").click(function(){
    owl.trigger('owl.next');
    })
    $(".owl-carousel-prev").click(function(){
    owl.trigger('owl.prev');
     })
 
    });
    </script>
</head>
<body>