<?php include('head.php'); ?>

<img class="genimg404 absolute top left none" src="images/404_pg.jpg" alt="" />

<section class="error404 Rumpelwhite center">
    <img class="bg " src="images/404_bg.jpg" alt="" />
    <div class="sos">
        <div class="img"><img src="images/lipuchkolet.png" alt="" /></div>
        <div class="text"><a href="/"><span>прислать за вами<br>липучколет</span></a></div>
    </div>
    <div class="center">
        <div class="title"><span>к сожалению эта страница не доступна!</span></div>
        <div class="text">
            <div class="left"><img src="images/404.png" alt="" /></div>
            <div class="right"><span>ой ой<br>Вы сбились с маршрута</span></div>
        </div>
    </div>
    <div class="logo"><img src="images/bunchems_planet.png" alt="" /></div>
    <div class="rights">2016. Все права защищены!</div>
</section>




<script src="js/jquery-1.11.1.min.js"></script>
<script>
$( document ).ready(function() {
    setTimeout(function() {
		window.location.href = "/";
	}, 5000);
});
</script>
<?php include('counters.php'); ?>